# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

from trimscape import settings


def set_profile(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    User = apps.get_model('auth', 'User')
    Profile = apps.get_model('riskscape_registration', 'Profile')
    for user in User.objects.all():
        profile = Profile()
        profile.full_name = user.first_name
        profile.institution = user.last_name
        profile.user = user
        profile.save()
        user.first_name = ''
        user.last_name = ''
        user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('riskscape_registration', '0002_profile'),
    ]

    operations = [
        migrations.RunPython(set_profile),
    ]
