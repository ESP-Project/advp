import tarfile
from datetime import datetime, date
import arrow
import os
from shutil import copyfile

from django.db import DataError
from django.db import IntegrityError
from django.db import transaction
from django.db.models import Count
from django_rq import job, enqueue
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.contrib.auth import get_user_model

from analysis import view_utils
from analysis.forms import FilterQueryForm, OutcomeQueryForm, GeoForm
from analysis.models import SummaryOutcome, Census, DASHBOARD_CONDITIONS, TimeseriesResultCache, MapResultCache, \
    CurrentRateResultCache, CensusTract
from patient_data.RiskscapeCopyMapping import RiskscapeCopyMapping
from patient_data.models import UploadData, TrimtrackerData, RiskscapeData
from trimscape.settings import DATA_SITE_COUNT, MEDIA_ROOT
from trimscape.settings import NEW_DATA_EMAIL_LIST, DEFAULT_FROM_EMAIL, ADMIN_EMAIL_LIST
from trimscape.settings import ORG_SYSTEM, MAIN_SITE_URL

def _user_email():
    User = get_user_model()
    users = User.objects.filter(is_active=True)
    email_list = []
    for u in users:
        if u.email:
            email_list.append(u.email)
    return email_list


def _send_email(error_message=None):
    # construct email
    if error_message:
        subject = "Data Upload Error on {}".format(ORG_SYSTEM)
        recipient_list = ADMIN_EMAIL_LIST
        from_email = DEFAULT_FROM_EMAIL
        context ={'site_name':ORG_SYSTEM,
                  'site_url':MAIN_SITE_URL,
                  'error_message': error_message}
        text_msg = render_to_string('load_error_email.txt', context)
        html_msg = render_to_string('load_error_email.html', context)
    else:
        subject = "New Data Upload on {}".format(ORG_SYSTEM)
        if len(NEW_DATA_EMAIL_LIST) > 0:
            recipient_list = NEW_DATA_EMAIL_LIST
        else:
            recipient_list = _user_email()
        from_email = DEFAULT_FROM_EMAIL
        context ={'site_name':ORG_SYSTEM, 'site_url':MAIN_SITE_URL}
        text_msg = render_to_string('new_data_email.txt', context)
        html_msg = render_to_string('new_data_email.html', context)
    # send email
    for to_email in recipient_list:
        send_mail(
            subject,
            text_msg,
            from_email,
            [to_email],
            fail_silently=False,
            html_message=html_msg,
        )

class LoadError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        site -- site that threw the error
        message -- explanation of the error
    """

    def __init__(self, site, message):
        self.site = site
        self.message = message


def process_data():
    loadable_data = UploadData.objects.filter(status=0)
    if len(loadable_data.values_list('site_name', flat=True)) == DATA_SITE_COUNT:
        _load_data.delay([ld.pk for ld in loadable_data])


@job('historical')
def _load_data(uploads):
    uploaded = len(uploads)
    for upload in uploads:
        ud = UploadData.objects.get(pk=upload)

        try:
            ud.status = 1
            ud.save()

            _untar(ud)

            with transaction.atomic():
                process_trimtracker(ud, ud.is_historic)
                ud.data_file.delete(save=True)
                ud.status = 2
                ud.save()
        except (LoadError, ValueError, DataError, IntegrityError) as e:
            ud.status = 3
            uploaded -= 1
            _send_email(error_message=str(e))
            raise LoadError(ud.site_name, e)
        finally:
            ud.processed = datetime.now()
            ud.save()

    if uploaded > 0:
        if settings.GEO_PLACES:
            view_utils.update_places()
        _update_outcomes()
        _update_timeseries_cache2()
        _update_catchment()
        _update_catchment2()
        _send_email()

def _untar(ud):
    if tarfile.is_tarfile(ud.data_file.path):
        backup_file = os.path.join(MEDIA_ROOT + 'upload', ud.site_name + '_backup.tbz2')
        copyfile(ud.data_file.path, backup_file)
        path_parts = ud.data_file.path.split('/')
        path = "{}/{}".format('/'.join(path_parts[:-1]), path_parts[-1:][0].split('.')[0])
        tar = tarfile.open(ud.data_file.path)
        tar_name = tar.getnames()[0]
        tar.extractall(path)
        tar.close()
        ud.data_file.delete(save=True)
        ud.data_file.name = "{}/{}".format(path, tar_name)
        ud.save()


def process_trimtracker(datafield, historic):
    copymap = RiskscapeCopyMapping(TrimtrackerData, datafield.data_file.path, TrimtrackerData.fieldmap(),
                                   static_mapping={'site': datafield.site_name})
    TrimtrackerData.objects.replace_data(datafield.site_name, copymap, historic)
    copymap.model = RiskscapeData
    copymap.mapping = RiskscapeData.fieldmap()
    RiskscapeData.objects.replace_data(datafield.site_name, copymap)
    #MapResultCache.objects.all().update(result=None)
    #CurrentRateResultCache.objects.all().update(result=None)
    MapResultCache.objects.all().delete()
    CurrentRateResultCache.objects.all().delete()


def _update_outcomes():
    SummaryOutcome.objects.exclude(field__in=[condition for condition in DASHBOARD_CONDITIONS]).delete()
    for condition, form_values in DASHBOARD_CONDITIONS.items():
        _update_outcome.delay(condition, form_values)


@job
def _update_outcome(condition, form_values):
    today = date.today()
    outcome_form = OutcomeQueryForm(form_values['outcome'])
    filter_form = FilterQueryForm(form_values['filter'])
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    else:
        print("{} error".format(condition))

    try:
        so = SummaryOutcome.current.get(field=condition,
                                        date__month=today.month,
                                        date__year=today.year)
    except SummaryOutcome.DoesNotExist:
        so = SummaryOutcome(field=condition)
    so.save()

def _update_timeseries_cache2(condition_name=None):
    TimeseriesResultCache.objects.all().delete()
    for condition, form_values in DASHBOARD_CONDITIONS.items():
        if condition_name is not None and not condition == condition_name:
            continue
        outcome_form = OutcomeQueryForm(form_values['outcome'])
        filter_form = FilterQueryForm(form_values['filter'])
        geo_form = GeoForm({"geo_area":settings.GEO_AREA_DEFAULT})
        geo_area = settings.GEO_AREA_DEFAULT
        stratify_by = ['none']
        if geo_area == 'zip' or geo_area == 'tract':
            geo_location = settings.GEO_LOCATION
        else:
            geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
        if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
            for stratify in stratify_by:
                data = [{'location': geo_location}]
                chunked = 'month'
                view_utils.timeseries(data,
                                       outcome_form,
                                       filter_form,
                                       geo_form,
                                       stratify,
                                       chunked,
                                       force_cache_update=True)

@transaction.atomic
def _bootstrap_outcome_cache(condition_name=None):
    dcl = []
    for condition, form_values in DASHBOARD_CONDITIONS.items():
        if condition_name is not None and not condition == condition_name:
            continue
        outcome_form = OutcomeQueryForm(form_values['outcome'])
        filter_form = FilterQueryForm(form_values['filter'])
        geo_form = GeoForm({"geo_area":settings.GEO_AREA_DEFAULT})
        geo_area = settings.GEO_AREA_DEFAULT
        stratify_by = ['none', 'age_group', 'sex', 'race_ethnicity']
        if geo_area == 'zip' or geo_area == 'tract':
            geo_location = settings.GEO_LOCATION
        else:
            geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
            stratify_by = ['none']
        if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
            for stratify in stratify_by:
                trc = TimeseriesResultCache.objects.cache_results(outcome_form=outcome_form,
                                                                  filter_form=filter_form,
                                                                  geo_form=geo_form,
                                                                  result=None,
                                                                  location=geo_location,
                                                                  stratify=stratify,
                                                                  time_chunk='month')
                dcl.append(trc.pk)
    return dcl

def _get_ratio(counts, idx):
    if counts[idx]['den'] == 0:
        return 0
    return float(counts[idx]['num'] * 100) / float(counts[idx]['den'])


@transaction.atomic
def _update_catchment():
    for loc in RiskscapeData.objects.filter(encounters_two_year__gt=0).values('census_id').order_by(
            'census_id').annotate(total=Count('census_id')).values('census_id', 'total'):
        try:
            cen = Census.objects.get(zip=loc['census_id'])
            cen.catchment = loc['total']
            cen.save()
        except Census.DoesNotExist:
            pass

@transaction.atomic
def _update_catchment2():
    for loc in RiskscapeData.objects.filter(encounters_two_year__gt=0).values('census_tract_id').order_by(
            'census_tract_id').annotate(total=Count('census_tract_id')).values('census_tract_id', 'total'):
        try:
            cen = CensusTract.objects.get(tract=loc['census_tract_id'])
            cen.catchment = loc['total']
            cen.save()
        except CensusTract.DoesNotExist:
            pass

