# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0018_auto_20170628_0953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='gonorrhea',
            field=models.PositiveSmallIntegerField(verbose_name='Gonorrhea', choices=[(0, 'No Test'), (1, 'Indeterminate'), (2, 'Negative'), (3, 'Positive')], blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='gonorrhea',
            field=models.PositiveSmallIntegerField(verbose_name='Gonorrhea', choices=[(0, 'No Test'), (1, 'Indeterminate'), (2, 'Negative'), (3, 'Positive')], blank=True, null=True),
        ),
    ]
