# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0019_auto_20170810_1405'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='riskscapedata',
            name='incoming_id',
        ),
        migrations.RemoveField(
            model_name='trimtrackerdata',
            name='incoming_id',
        ),
    ]
