# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0004_auto_20161208_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploaddata',
            name='data_type',
            field=models.CharField(max_length=1, choices=[('W', 'Weekly'), ('M', 'Monthly')]),
        ),
    ]
