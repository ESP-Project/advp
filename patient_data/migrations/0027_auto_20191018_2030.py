# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-10-18 20:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0026_auto_20190716_1249'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskscapedata',
            name='primary_payer',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'Not Medicaid'), (1, 'Medicaid'), (2, 'Unknown')], null=True, verbose_name='Primary Payer'),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='primary_payer',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'Not Medicaid'), (1, 'Medicaid'), (2, 'Unknown')], null=True, verbose_name='Primary Payer'),
        ),
    ]
