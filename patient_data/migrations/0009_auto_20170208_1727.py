# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import patient_data.models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0008_auto_20170205_0742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='curr_flu_vac',
            field=models.PositiveSmallIntegerField(blank=True, verbose_name='Flu Vaccine', choices=[(1, 'Yes'), (0, 'No')], null=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='curr_flu_vac',
            field=models.PositiveSmallIntegerField(blank=True, verbose_name='Flu Vaccine', choices=[(1, 'Yes'), (0, 'No')], null=True),
        ),
        migrations.AlterField(
            model_name='uploaddata',
            name='data_file',
            field=models.FileField(upload_to=patient_data.models.path_and_rename, max_length=255),
        ),
    ]
