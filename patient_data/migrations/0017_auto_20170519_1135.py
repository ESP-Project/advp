# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0016_auto_20170424_0918'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskscapedata',
            name='lyme_cum',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], blank=True, verbose_name='Lyme cumulative (this year)', null=True),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='lyme_cum',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], blank=True, verbose_name='Lyme cumulative (this year)', null=True),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='lyme',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], blank=True, verbose_name='Lyme monthly', null=True),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='lyme_last',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], blank=True, verbose_name='Lyme cumulative (last year)', null=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='lyme',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], blank=True, verbose_name='Lyme monthly', null=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='lyme_last',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], blank=True, verbose_name='Lyme cumulative (last year)', null=True),
        ),
    ]
