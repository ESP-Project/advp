from django import forms
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.conf import settings

from modelqueryform.forms import ModelQueryForm

from patient_data.models import PatientData

GEO_AREA_CHOICES = (
            ('zip',('zip code')),
            ('tract',('census tract')),
        )

if settings.GEO_PLACE_CHOICES:
    GEO_AREA_CHOICES = settings.GEO_PLACE_CHOICES

class GeoForm(forms.Form):
        geo_area = forms.ChoiceField(
            widget=forms.RadioSelect(),
            choices=GEO_AREA_CHOICES,
            initial=settings.GEO_AREA_DEFAULT,
        )

class ConcatPrintForm(ModelQueryForm):
    def print_age_group(self, field, values):
        return self._concat_values_display(field, values)

    def print_ldl(self, field, values):
        return self._concat_values_display(field, values)

    def print_tg(self, field, values):
        return self._concat_values_display(field, values)

    def print_a1c(self, field, values):
        return self._concat_values_display(field, values)

    def print_bmi_percent(self, field, values):
        return self._concat_values_display(field, values)

    def print_sbp_percent(self, field, values):
        return self._concat_values_display(field, values)

    def print_dbp_percent(self, field, values):
        return self._concat_values_display(field, values)

    def _get_single_choice_display(self, choices, value):
        return str([x[1] for x in choices if x[0] == int(value)][0])

    def _concat_values_display(self, field, values):
        choices = PatientData._meta.get_field(field.name).choices
        if (len(values)) == 1:
            return self._get_single_choice_display(choices, values[0])

        display = []
        if values[0] == '0':
            display.append(self._get_single_choice_display(choices, '0'))
            values = values[1:]
        inclusive_values = range(int(values[0]), int(values[-1]) + 1)
        if values == [str(x) for x in inclusive_values] and (int(values[0]) == choices[0][0] or
                                                                     int(values[-1]) == choices[-1][0]):
            if int(values[0]) == choices[0][0]:
                display.append("&lt;{}".format(choices[len(values)][1].split("-")[0]))
            if int(values[-1]) == choices[-1][0]:
                display.append("&ge;{}".format(choices[len(values) * -1][1].split("-")[0]))
        else:
            display.append(self._get_single_choice_display(choices, inclusive_values[0]))
            for iv in inclusive_values[1:]:
                if str(iv) in values:
                    if display[-1] is None:
                        display[-1] = self._get_single_choice_display(choices, iv)
                    else:
                        try:
                            display[-1] = "{}-{}".format(display[-1].split("-")[0],
                                                         self._get_single_choice_display(choices, iv).split("-")[1])
                        except IndexError:
                            display[-1] = "{}-{}".format(display[-1].split("-")[0],
                                                         self._get_single_choice_display(choices, iv))

                else:
                    if display[-1] is not None:
                        display.append(None)

        return ",".join(display)


class OutcomeQueryForm(ConcatPrintForm):
    model = PatientData
    include = ['dm1',
               'dm2',
               'pre_dm',
               'diagnosed_diabetes',
               'bmi',
               'hypertension',
               'diagnosed_hypertension',
               'cvd',
               'smoker',
               'asthma',
               'depression',
               'bmi_percent',
               'sbp_percent',
               'dbp_percent',
               'ili_current',
               'ili_cum',
               'curr_flu_vac',
               'lyme',
               'lyme_last',
               'lyme_cum',
               'pertussis',
               'pertussis_last',
               'tdap',
               'chlamydia',
               'gonorrhea',
               'syph_test',
               'hepc_test',
               'ldl',
               'tg',
               'a1c',
               'opioid_rx',
               'benzo_rx',
               'high_opiod_rx',
               'opioid_benzo_concurrent',
               'pregnant',
               'gdm',
               ]
    
    def __init__(self, *args, **kwargs):
        is_auth = kwargs.pop('is_auth') if 'is_auth' in kwargs.keys() else False
        super(OutcomeQueryForm, self).__init__(*args, **kwargs)
        if not is_auth:
            for auth_outcome in settings.AUTH_OUTCOMES:
                self.fields.pop(auth_outcome)

ENCOUNTER_CHOICES = (
    ('Historic', mark_safe('&ge;1 in the past 2 years')),
    ('Current', mark_safe('&ge;1 in the past 1 year')),
    ('Repeat', mark_safe('&ge;2 in the past 2 years')),
    ('Regular', mark_safe('&ge;2 in the past 1 year')),
)

class FilterQueryForm(ConcatPrintForm):
    
    model = PatientData
    include = ['site',
               'age_group',
               'sex',
               'race_ethnicity',
               'ethnicity',
               'birth_cohort',
               'primary_payer',
               'smoker',
               'pregnant',
               'bmi',
               'bmi_percent',
               'gdm',
               'pre_dm',
               'dm1',
               'dm2',
               'diagnosed_diabetes',
               'hypertension',
               'diagnosed_hypertension',
               'cvd',
               'asthma',
               'depression',
               'lyme',
               'lyme_last',
               'lyme_cum',
               'pertussis',
               'pertussis_last',
               'ili_current',
               'ili_cum',
               'curr_flu_vac',
               'ldl',
               'tg',
               'a1c',
               'chlamydia',
               'gonorrhea',
               'hepc_test',
               'syph_test',
               'sysbp',
               'sbp_percent',
               'diabp',
               'dbp_percent',
               'insulin',
               'metformin',
               'opioid_rx',
               'high_opiod_rx',
               'benzo_rx',
               'opioid_benzo_concurrent',
               'tdap',
               'tdap_preg',
               'encounters_total',
               ]

    def __init__(self, *args, **kwargs):
        is_auth = kwargs.pop('is_auth') if 'is_auth' in kwargs.keys() else False
        super(FilterQueryForm, self).__init__(*args, **kwargs)
        self.fields['encounters_one_year'] = forms.ChoiceField(
            widget=forms.Select(attrs={'class': 'custom-select form-control'}),
            label='Recent Encounters',
            choices=ENCOUNTER_CHOICES,
        )
        if not is_auth:
            for auth_outcome in settings.AUTH_OUTCOMES:
                self.fields.pop(auth_outcome)

    def filter_encounters_one_year(self, field_name, values):
        if values == "Repeat":
            return Q(**{'encounters_two_year__gte': 2})
        elif values == "Current":
            return Q(**{'encounters_one_year__gte': 1})
        elif values == "Historic":
            return Q(**{'encounters_two_year__gt': 0})
        elif values == "Regular":
            return Q(**{'encounters_one_year__gte': 2})

    def print_encounters_one_year(self, field, values):
        return "{}".format([enc[1] for enc in ENCOUNTER_CHOICES if enc[0] == values][0])

    def build_encounters_total(self, model_field):
        return forms.ChoiceField(
            label=model_field.verbose_name,
            widget=forms.Select(attrs={'class': 'custom-select form-control'}),
            choices=(
                ('1', mark_safe('&ge;1')),
                ('2', mark_safe('&ge;2')),
                ('3', mark_safe('&ge;3')),
                ('4', mark_safe('&ge;4')),
                ('5', mark_safe('&ge;5')),
                ('10', mark_safe('&ge;10')),
            )
        )

    def filter_encounters_total(self, field_name, values):
        return Q(**{field_name + '__gte': values})

    def print_encounters_total(self, field, values):
        return "&ge;{}".format(values)

    def pretty_print_query(self):
        print_fields = self.changed_data.copy()
        if set(['encounters_total', 'encounters_one_year']).issubset(set(print_fields)):
            if (self.cleaned_data['encounters_one_year'] in ['Current', 'Historic'] and \
                            self.cleaned_data['encounters_total'] == '1') \
                    or self.cleaned_data['encounters_total'] in ['1', '2']:
                print_fields.remove('encounters_total')
        return super().pretty_print_query(print_fields)


def process_queryform(request, outcome=None):
    from analysis.models import DASHBOARD_CONDITIONS
    is_auth_group = request.user.groups.filter(name__in=settings.AUTH_GROUPS).exists()
    is_auth = is_auth_group or request.user.is_superuser
    if request.method == 'POST':
        outcome_form = OutcomeQueryForm(request.POST, prefix="outcome-form", is_auth=is_auth)
        filter_form = FilterQueryForm(request.POST, prefix="filter-form", is_auth=is_auth)
        geo_form = GeoForm(request.POST)
    else:
        geo_form = GeoForm({"geo_area":settings.GEO_AREA_DEFAULT})
        if outcome is not None:
            outcome_form_data = {}
            filter_form_data = {}
            for key, value in DASHBOARD_CONDITIONS[outcome]['outcome'].items():
                outcome_form_data['outcome-form-' + key] = value
            for key, value in DASHBOARD_CONDITIONS[outcome]['filter'].items():
                filter_form_data['filter-form-' + key] = value
            outcome_form = OutcomeQueryForm(outcome_form_data, prefix="outcome-form", is_auth=is_auth)
            filter_form = FilterQueryForm(filter_form_data, prefix="filter-form", is_auth=is_auth)
        else:
            outcome_form = OutcomeQueryForm(prefix="outcome-form", is_auth=is_auth)
            filter_form = FilterQueryForm(prefix="filter-form", is_auth=is_auth)

    return outcome_form, filter_form, geo_form


def reinitialize(request, formdata):
    initial = {x: y for x, y in formdata.cleaned_data.items() if y}
    is_auth_group = request.user.groups.filter(name__in=settings.AUTH_GROUPS).exists()
    is_auth = is_auth_group or request.user.is_superuser
    if formdata.prefix == "outcome-form":
        return OutcomeQueryForm(initial=initial, prefix=formdata.prefix, is_auth=is_auth)
    elif formdata.prefix == "filter-form":
        return FilterQueryForm(initial=initial, prefix=formdata.prefix, is_auth=is_auth)
