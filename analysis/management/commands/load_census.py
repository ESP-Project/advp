import csv, os, sys

from analysis.models import Census, CensusTract, City
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--zip',
            action='store_true',
            help='Load zip codes and population',
        )
        parser.add_argument(
            '--tract',
            action='store_true',
            help='Load census tracts and population',
        )
        parser.add_argument(
            '--cityzip',
            action='store_true',
            help='Load city zip codes',
        )
        parser.add_argument(
            '--citytract',
            action='store_true',
            help='Load city census tracts',
        )

    def handle(self, *args, **options):
        census_dir = os.path.join(settings.MEDIA_ROOT, "census/")
        if options['zip'] or options['tract']:
            load_file = 'zip_pop.csv' if options['zip'] else 'tract_pop.csv'
            fpath = census_dir + load_file
            if not os.path.isfile(fpath):
                self.stderr.write('"%s" does not exist.' % fpath)
                self.stderr.write('zip code (or census tract) and population data is required.')
                sys.exit() # exit for migration
            else:
                f = open(fpath, "r")
                reader = csv.reader(f)
                if options['zip']:
                    self.stdout.write('loading zip codes and population')
                else :
                    self.stdout.write('loading census tracts and population')
                for row in reader:
                    pop = row[1] if row[1] else None
                    if options['zip']:
                        c, created = Census.objects.get_or_create(zip=row[0])
                        c.population = pop
                        c.save()
                    else:
                        c, created = CensusTract.objects.get_or_create(tract=row[0])
                        c.population = pop
                        c.save()
        elif options['cityzip']:
            load_file = 'city_zip.csv'
            fpath = census_dir + load_file
            if not os.path.isfile(fpath):
                self.stderr.write('"%s" does not exist.' % fpath)
            else:
                f =open(fpath, "r")
                reader = csv.reader(f)
                self.stdout.write('loading cities and zip codes')
                for row in reader:
                    Census.objects.get_or_create(zip=row[1])
                    c, created = City.objects.get_or_create(city=row[0])
                    c.zips.add(row[1])
                    c.save
        elif options['citytract']:
            load_file = 'city_tract.csv'
            fpath = census_dir + load_file
            if not os.path.isfile(fpath):
                self.stderr.write('"%s" does not exist.' % fpath)
            else:
                f = open(fpath, "r")
                reader = csv.reader(f)
                self.stdout.write('loading cities and census tracts')
                for row in reader:
                    CensusTract.objects.get_or_create(tract=row[1])
                    c, created = City.objects.get_or_create(city=row[0])
                    c.tracts.add(row[1])
                    c.save
        else:
            self.stderr.write('Arguments must be included. See --help')
