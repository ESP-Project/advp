# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0022_auto_20170816_1728'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'ordering': ['city']},
        ),
    ]
