# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-19 20:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0027_auto_20180419_1828'),
    ]

    operations = [
        migrations.AddField(
            model_name='currentrateresultcache',
            name='filter_hash',
            field=models.CharField(default='a', max_length=32),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='currentrateresultcache',
            name='outcome_hash',
            field=models.CharField(default='a', max_length=32),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mapresultcache',
            name='filter_hash',
            field=models.CharField(default='a', max_length=32),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mapresultcache',
            name='outcome_hash',
            field=models.CharField(default='a', max_length=32),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='timeseriesresultcache',
            name='filter_hash',
            field=models.CharField(default='a', max_length=32),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='timeseriesresultcache',
            name='outcome_hash',
            field=models.CharField(default='a', max_length=32),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='currentrateresultcache',
            name='filter_form',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='currentrateresultcache',
            name='outcome_form',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='mapresultcache',
            name='filter_form',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='mapresultcache',
            name='outcome_form',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='timeseriesresultcache',
            name='filter_form',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterField(
            model_name='timeseriesresultcache',
            name='outcome_form',
            field=models.JSONField(default=dict),
        ),
        migrations.AlterUniqueTogether(
            name='currentrateresultcache',
            unique_together=set([('outcome_hash', 'filter_hash', 'location', 'stratify')]),
        ),
        migrations.AlterUniqueTogether(
            name='mapresultcache',
            unique_together=set([('outcome_hash', 'filter_hash')]),
        ),
        migrations.AlterUniqueTogether(
            name='timeseriesresultcache',
            unique_together=set([('outcome_hash', 'filter_hash', 'location', 'stratify', 'time_chunk')]),
        ),
    ]
