# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0007_auto_20161020_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='resultcache',
            name='stratify',
            field=models.CharField(max_length=16, null=True, blank=True),
        ),
    ]
