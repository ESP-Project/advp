# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Census',
            fields=[
                ('zip', models.CharField(max_length='5', serialize=False, primary_key=True)),
                ('population', models.PositiveIntegerField(null=True, blank=True)),
                ('catchment', models.PositiveIntegerField(null=True, blank=True)),
                ('coverage', models.CharField(default='Unknown', max_length=7)),
            ],
            options={
                'ordering': ['zip'],
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('city', models.CharField(max_length=16, serialize=False, primary_key=True)),
                ('zips', models.ManyToManyField(to='analysis.Census')),
            ],
        ),
        migrations.CreateModel(
            name='SummaryOutcome',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(auto_now=True)),
                ('field', models.CharField(max_length=50)),
                ('value', models.FloatField()),
                ('most_recent_val', models.FloatField(null=True, verbose_name='6M', blank=True)),
                ('next_most_recent_val', models.FloatField(null=True, verbose_name='1Y', blank=True)),
                ('least_recent_val', models.FloatField(null=True, verbose_name='2Y', blank=True)),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.AlterUniqueTogether(
            name='summaryoutcome',
            unique_together=set([('field', 'date')]),
        ),
    ]
