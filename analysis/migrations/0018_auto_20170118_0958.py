# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_coverage(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Census = apps.get_model("analysis", "Census")
    for census in Census.objects.all():
        census.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0017_auto_20170116_0858'),
    ]

    operations = [
        migrations.RunPython(update_coverage),
    ]
