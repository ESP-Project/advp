# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0013_auto_20161025_1938'),
    ]

    operations = [
        migrations.CreateModel(
            name='CurrentRateResultCache',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('outcome', models.JSONField(default=dict)),
                ('filter', models.JSONField(default=dict)),
                ('result', models.JSONField(null=True, blank=True)),
                ('viewed', models.IntegerField(default=1)),
                ('location', models.CharField(max_length=25)),
                ('stratify', models.CharField(max_length=16, null=True, blank=True)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='currentrateresultcache',
            unique_together=set([('outcome', 'filter', 'location', 'stratify')]),
        ),
    ]
