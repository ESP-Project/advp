# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

NEIGHBORHOODS = (
    ('Boston - Allston/Brighton', ['02134', '02135', '02136']),
    ('Boston - Back Bay', ['02108', '02109', '02110', '02113', '02114', '02116', '02199']),
    ('Boston - North End', ['02113']),
    ('Boston - Charlestown', ['02129']),
    ('Boston - East Boston', ['02128']),
    ('Boston - Fenway', ['02115', '02215']),
    ('Boston - Hyde Park', ['02136']),
    ('Boston - Jamaica Plain', ['02130']),
    ('Boston - Mattapan', ['02126']),
    ('Boston - North Dorchester', ['02121', '02125']),
    ('Boston - Roslindale', ['02131']),
    ('Boston - Roxbury', ['02119', '02120']),
    ('Boston - South Boston', ['02127', '02210']),
    ('Boston - South Dorchester', ['02122', '02124']),
    ('Boston - South End', ['02111', '02118']),
    ('Boston - Chinatown', ['02111']),
    ('Boston - West Roxbury', ['02132']),
)


def populate_neigborhoods(apps, schema_editor):
    City = apps.get_model("analysis", "City")
    for area in NEIGHBORHOODS:
        c, created = City.objects.get_or_create(city=area[0])
        c.zips.add(*area[1])
        c.save()


class Migration(migrations.Migration):
    dependencies = [
        ('analysis', '0021_auto_20170816_1726'),
    ]

    def reverse_func(apps, schema_editor):
        # forwards_func() creates two Country instances,
        # so reverse_func() should delete them.
        City = apps.get_model("analysis", "City")
        for area in NEIGHBORHOODS:
            try:
                City.objects.get(city=area[0]).delete()
            except City.DoesNotExist:
                pass

    operations = [
        migrations.RunPython(populate_neigborhoods, reverse_func),
    ]
