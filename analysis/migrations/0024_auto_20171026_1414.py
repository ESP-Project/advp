# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0023_auto_20170823_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='currentrateresultcache',
            name='last_accessed',
            field=models.DateField(auto_created=True, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='mapresultcache',
            name='last_accessed',
            field=models.DateField(auto_created=True, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='timeseriesresultcache',
            name='last_accessed',
            field=models.DateField(auto_created=True, null=True, blank=True),
        ),
    ]
