# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-19 18:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0026_auto_20180419_1824'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currentrateresultcache',
            name='filter_form',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='currentrateresultcache',
            name='outcome_form',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='mapresultcache',
            name='filter_form',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='mapresultcache',
            name='outcome_form',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='timeseriesresultcache',
            name='filter_form',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='timeseriesresultcache',
            name='outcome_form',
            field=models.CharField(max_length=32),
        ),
    ]
