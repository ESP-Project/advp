from math import ceil, floor

from django_rq import job
from django.conf import settings

from analysis.forms import FilterQueryForm
from analysis.forms import OutcomeQueryForm
from analysis.models import MapResultCache, CurrentRateResultCache, TimeseriesResultCache
from analysis.view_utils import census_satisfied, set_decimals, filter_by_location
from analysis.view_utils import sufficient_data, get_data_time_range
from patient_data.models import RiskscapeData, TrimtrackerData

@job
def generate_map_cache(cache_pk, geo_area, month=None, year=None):
    mrc = MapResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(mrc.outcome, is_auth=True)
    filter_form = FilterQueryForm(mrc.filter, is_auth=True)
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    map_data = []
    md_range = {"min": float(100), "max": float(0)}
    statewide = {"numerator": 0, "denominator": 0}

    geo_place_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    geo_place_id = settings.GEO_PLACES[geo_area]['GEO_PLACE_REFID'] if settings.GEO_PLACES else None
    if month and year:
        data_model = TrimtrackerData
        if geo_place_id:
            filtered_model = data_model.objects.filter(place=geo_place_id, year=year, month=month)
        else:
            filtered_model = data_model.objects.filter(year=year, month=month)
    else:
        data_model = RiskscapeData
        if geo_place_id:
            filtered_model = data_model.objects.filter(place=geo_place_id)
        else:
            filtered_model = data_model.objects.all()
    
    if (geo_area == 'tract') or (geo_place_type == 'tract'):
        geo_codes = data_model.objects.qs_ratio(
            qs=filter_form.process(
                filtered_model.filter(census_tract__population__isnull=False)
            ).values('census_tract', 'census_tract__coverage', 'census_tract__catchment', 'census_tract__population'),
            outcome_filters=outcome_form)
    else:
        geo_codes = data_model.objects.qs_ratio(
            qs=filter_form.process(
                filtered_model.filter(census__population__isnull=False)
            ).values('census', 'census__coverage', 'census__catchment', 'census__population'),
            outcome_filters=outcome_form)

    for zip_code in geo_codes:
        statewide["numerator"], statewide["denominator"] = statewide["numerator"] + zip_code["numerator"], \
                                                           statewide["denominator"] + zip_code["denominator"]
        if not census_satisfied(zip_code):
            if zip_code['denominator'] > 99 and not settings.MENDS:
                if zip_code['numerator'] > 0:
                    prevalence = (100.0 / float(zip_code['denominator']))
                    numerator = None
                    denominator = None
                else:
                    prevalence = 0
                    numerator = 0
                    denominator = zip_code['denominator']
            else:
                prevalence = None
                numerator = None
                denominator = None
        else:
            prevalence = (float(zip_code['numerator']) * 100.0 / float(zip_code['denominator']))
            numerator = zip_code['numerator']
            denominator = zip_code['denominator']
            md_range['min'], md_range['max'] = min(prevalence, md_range['min']), max(prevalence, md_range['max'])

        if (geo_area == 'tract') or (geo_place_type =='tract'):
            map_data.append(
                {'zip': zip_code['census_tract'], 'coverage': zip_code['census_tract__coverage'],
                'population': zip_code['census_tract__population'], 'catchment': zip_code['census_tract__catchment'],
                'prevalence': prevalence, 'numerator': numerator, 'denominator': denominator})
        else:
            map_data.append(
                {'zip': zip_code['census'], 'coverage': zip_code['census__coverage'],
                'population': zip_code['census__population'], 'catchment': zip_code['census__catchment'],
                'prevalence': prevalence, 'numerator': numerator, 'denominator': denominator})

    if md_range['max'] == md_range['min'] == float(100):
        md_range['min'] = 0

    if md_range['max'] > md_range['min']:
        for md in map_data:
            if md['prevalence'] is not None:
                md['normalized'] = (md['prevalence'] - md_range['min']) / (md_range['max'] - md_range['min'])
                md['prevalence'] = set_decimals(md['prevalence'])
    else:
        md_range['min'] = md_range['max']
    md_range['max'] = int(ceil(md_range['max']))
    md_range['min'] = int(floor(md_range['min']))
    try:
        statewide['ratio'] = set_decimals(float(statewide['numerator']) * 100.0 / float(statewide['denominator']))
    except ZeroDivisionError:
        statewide['ratio'] = 0

    result = {"map_data": map_data,
              "map_range": md_range,
              "statewide": statewide}

    mrc.result = result
    mrc.save()

def save_strat_result(crc, result, strat_id):
    if strat_id == 1:
        crc_stratify = crc.stratify
    elif strat_id == 2:
        crc_stratify = crc.stratify2
    elif strat_id == 3:
        crc_stratify = crc.stratify
        crc_stratify2 = crc.stratify2
    else:
        return False

    for strat in result:
        if (crc_stratify in strat):
            strat_name = None
            for v in RiskscapeData._meta.get_field(crc_stratify).choices:
                if v[0] == strat[crc_stratify]:
                    strat[crc_stratify] = v[1]
                    break
            if strat[crc_stratify] is None:
                strat[crc_stratify] = 'Unknown'
        else:
            strat[crc_stratify] = 'Unknown'

        if (strat_id == 3) and (crc_stratify2 in strat):
            strat_name = None
            for v in RiskscapeData._meta.get_field(crc_stratify2).choices:
                if v[0] == strat[crc_stratify2]:
                    strat[crc_stratify2] = v[1]
                    break
            if strat[crc_stratify2] is None:
                strat[crc_stratify2] = 'Unknown'
        elif (strat_id == 3) and not (crc_stratify2 in strat):
            strat[crc_stratify2] = 'Unknown'
    if sufficient_data(result):
        strat_name = crc_stratify
        if strat_id == 3:
            strat_name = crc_stratify + " & " + crc_stratify2
        all_strats = {strat_name: 'All', 'numerator': 0, 'denominator': 0}
        for item in result:
            all_strats['numerator'] = all_strats['numerator'] + item['numerator']
            all_strats['denominator'] = all_strats['denominator'] + item['denominator']
            if not census_satisfied(item):
                item['numerator'] = -1
                item['denominator'] = -1

        if strat_id == 1:
            try:
                crc.result = [dict(x) for x in result if x['denominator'] > -1]
                if not crc.stratify == 'none':
                    crc.result.append(all_strats)
                else:
                    all_strats['none'] = "Unstratified"
                    crc.result = all_strats
            except TypeError:
                crc.result = False

        elif strat_id == 2:
            try:
                crc.result2 = [dict(x) for x in result if x['denominator'] > -1]
                if not crc.stratify2 == 'none':
                    crc.result2.append(all_strats)
                else:
                    all_strats['none'] = "Unstratified"
                    crc.result2 = all_strats
            except TypeError:
                crc.result2 = False

        elif strat_id == 3:
            try:
                crc.result3 = [dict(x) for x in result if x['denominator'] > -1]
                if not crc.stratify2 == 'none':
                    crc.result3.append(all_strats)
                else:
                    all_strats['none'] = "Unstratified"
                    crc.result3 = all_strats
            except TypeError:
                crc.result3 = False
        else:
            return False

@job
def generate_current_rate_cache(cache_pk, geo_area, month, year):

    crc = CurrentRateResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(crc.outcome, is_auth=True)
    filter_form = FilterQueryForm(crc.filter, is_auth=True)
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    if month and year:
        data_model = TrimtrackerData
    else:
        data_model = RiskscapeData

    filtered = filter_by_location(crc.location, data_model, filter_form, geo_area, year, month)

    # stratification 1
    strat1 = 'age_group' if crc.stratify == 'none' else crc.stratify
    result1 = data_model.objects.qs_ratio(filtered.values(strat1), outcome_form).distinct().order_by(strat1)
    save_strat_result(crc, result1, 1)

    # stratification 2
    strat2 = crc.stratify2
    if strat2 != 'none':
        result2 = data_model.objects.qs_ratio(filtered.values(strat2), outcome_form).distinct().order_by(strat2)
        save_strat_result(crc, result2, 2)
        result3 = data_model.objects.qs_ratio(filtered.values(strat1,strat2), outcome_form).distinct().order_by(strat1,strat2)
        save_strat_result(crc, result3, 3)

    crc.save()

@job
def generate_timeseries_cache(cache_pk, geo_area):
    trc = TimeseriesResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(trc.outcome, is_auth=True)
    filter_form = FilterQueryForm(trc.filter, is_auth=True)
    if outcome_form.is_valid() and filter_form.is_valid():
        pass

    filtered = filter_by_location(trc.location, TrimtrackerData, filter_form, geo_area)
    has_census = False
    geo_place_id = settings.GEO_PLACES[geo_area]['GEO_PLACE_REFID'] if settings.GEO_PLACES else None
    month_range, quarter_range, year_range, *extra = get_data_time_range(geo_place_id)
    if not trc.stratify == 'none':
        if trc.time_chunk == 'month':
            filtered = filtered.values(trc.stratify, 'year', 'month').order_by('year', 'month', trc.stratify)
            range = month_range
        elif trc.time_chunk == "year":
            filtered = filtered.values(trc.stratify, 'year').order_by('year', trc.stratify)
            range = year_range
        elif trc.time_chunk == "quarter":
            filtered = filtered.values(trc.stratify, 'year', 'quarter').order_by('year', 'quarter', trc.stratify)
            range = quarter_range

        result = {}
        strat_choices = filter_form.model._meta.get_field(trc.stratify).choices
        if filter_form.cleaned_data[trc.stratify]:
            strat_choices = [choice for choice in strat_choices if str(choice[0]) in filter_form.cleaned_data[trc.stratify]]
        for strat in [x[1] for x in strat_choices]:
            irange = iter(range)
            result[strat] = []
            while True:
                try:
                    calendar = next(irange)
                    result[strat].append(_get_filler_point(calendar, trc.time_chunk))
                except StopIteration:
                    break

        for point in TrimtrackerData.objects.qs_ratio(filtered, outcome_form):
            if point[trc.stratify] is None:
                del point[trc.stratify]
                continue
            strat = [x[1] for x in filter_form.model._meta.get_field(trc.stratify).choices
                     if x[0] == point[trc.stratify]][0]
            del point[trc.stratify]

            if not census_satisfied(point):
                point['numerator'] = -1
                point['denominator'] = -1
            else:
                has_census = True
            for filler in result[strat]:
                if filler['year'] == point['year'] and filler[trc.time_chunk] == point[trc.time_chunk]:
                    filler['numerator'], filler['denominator'] = point['numerator'], point['denominator']
                    break
    else:
        if trc.time_chunk == "month":
            filtered = filtered.values('year', 'month').order_by('year', 'month')
            range = month_range
        if trc.time_chunk == "year":
            filtered = filtered.values('year').order_by('year')
            range = year_range
        elif trc.time_chunk == "quarter":
            filtered = filtered.values('year', 'quarter').order_by('year', 'quarter')
            range = quarter_range
        result = {'All': []}

        irange = iter(range)
        result = {'All': []}
        while True:
            try:
                calendar = next(irange)
                result['All'].append(_get_filler_point(calendar, trc.time_chunk))
            except StopIteration:
                break
        for point in TrimtrackerData.objects.qs_ratio(filtered, outcome_form):
            if not census_satisfied(point):
                point['numerator'] = -1
                point['denominator'] = -1
            else:
                has_census = True
            for filler in result['All']:
                if filler['year'] == point['year'] and filler[trc.time_chunk] == point[trc.time_chunk]:
                    filler['numerator'], filler['denominator'] = point['numerator'], point['denominator']
                    break

    if not has_census:
        result = []
    try:
        trc.result = [dict(x) for x in result]
    except ValueError:
        trc.result = result

    trc.save()


def _match_calendar_timeseries(calendar, point, chunk):
    if calendar.year != point['year']:
        return False
    if chunk == 'month':
        if calendar.month != point['month']:
            return False
    elif chunk == 'quarter':
        if calendar.quarter != point['quarter']:
            return False
    return True


def _get_filler_point(calendar, chunk):
    return {"year": calendar.year, chunk: getattr(calendar, chunk), 'numerator': -1, 'denominator': -1}
