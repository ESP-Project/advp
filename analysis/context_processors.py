import arrow

from analysis.models import SummaryOutcome, Census, City, CensusTract, DASHBOARD_CONDITIONS
from analysis.view_utils import get_quarter, get_data_time_range
from patient_data.models import UploadData
from trimscape import utils
from django.conf import settings

def geo_config(request):

    return {'GEO_FILE': settings.GEO_FILE,
            'GEO_AREA': settings.GEO_AREA,
            'GEO_ZOOM': settings.GEO_ZOOM,
            'GEO_CENTER_LAT': settings.GEO_CENTER_LAT,
            'GEO_CENTER_LON': settings.GEO_CENTER_LON,
            'GEO_BOUNDS_LAT1': settings.GEO_BOUNDS_LAT1,
            'GEO_BOUNDS_LON1': settings.GEO_BOUNDS_LON1,
            'GEO_BOUNDS_LAT2': settings.GEO_BOUNDS_LAT2,
            'GEO_BOUNDS_LON2': settings.GEO_BOUNDS_LON2,
            'GEO_LOCATION': settings.GEO_LOCATION,
            'GEO2_FILE': settings.GEO2_FILE,
            'GEO2_AREA': settings.GEO2_AREA,
            'GEO2_ZOOM': settings.GEO2_ZOOM,
            'GEO2_CENTER_LAT': settings.GEO2_CENTER_LAT,
            'GEO2_CENTER_LON': settings.GEO2_CENTER_LON,
            'GEO2_BOUNDS_LAT1': settings.GEO2_BOUNDS_LAT1,
            'GEO2_BOUNDS_LON1': settings.GEO2_BOUNDS_LON1,
            'GEO2_BOUNDS_LAT2': settings.GEO2_BOUNDS_LAT2,
            'GEO2_BOUNDS_LON2': settings.GEO2_BOUNDS_LON2,
            'GEO2_LOCATION': settings.GEO2_LOCATION,
            'GEO_PLACES': settings.GEO_PLACES}

def current_outcomes(request):
    is_auth = request.user.groups.filter(name__in=settings.AUTH_GROUPS).exists()
    values = SummaryOutcome.current.all()
    outcomes = {}

    try:
        for v in values:
            if v.field in DASHBOARD_CONDITIONS:
                db_outcome = DASHBOARD_CONDITIONS[v.field]['outcome']
                auth_outcome = False
                for outcome in settings.AUTH_OUTCOMES:
                    if outcome in db_outcome:
                        auth_outcome = True
                if not is_auth and auth_outcome:
                    continue
            
            try:
                outcome = {'name': v.field,
                           'display_name': v.display_name,
                           'interest': v.values_of_interest,
                           'value': v.this_month,
                           'trendline': v.trendline,
                           'chart_type': v.chart_type
                           }
                outcomes[v.field] = outcome
            except TypeError:
                pass

        donuts = [outcomes.get("Type 2 Diabetes", None),
                  outcomes.get("Smoking", None),
                  outcomes.get("Hypertension", None),
                  outcomes.get("Pediatric Asthma", None),
                  outcomes.get("Obesity", None),
                  outcomes.get("Overweight", None),
                  outcomes.get("Curr ILI vac", None),
                  outcomes.get("Depression", None),
                  outcomes.get("Opioid Rx", None),
                  outcomes.get("Syphilis Test Pregnant", None),
                  outcomes.get("Hepc Boomer", None),
                  outcomes.get("Young Female Chlamy", None)]
        donuts = [x for x in donuts if x]

        donuts = utils.pad_dashboard(donuts)

        sparks = [outcomes.get("Influenza like illness Monthly", None),
                  outcomes.get("Lyme disease Monthly", None),
                  outcomes.get("Pertussis syndrome Monthly", None),
                  outcomes.get("Influenza like illness Cumulative", None),
                  outcomes.get("Lyme disease Cumulative", None),
                  outcomes.get("Pertussis syndrome Cumulative", None)]
        sparks = [x for x in sparks if x]
        sparks = utils.pad_dashboard(sparks)

        cards = donuts + sparks
        if cards[-3:] == [None, None, None]:
            cards = cards[:-3]
        return {"outcomes": utils.nest(cards)}
    except (AttributeError, TypeError):
        return {}


def last_outcome_update(request):
    lou = {"last_outcome_update": None}
    try:
        lou["last_outcome_update"] = SummaryOutcome.current.first().date
    except AttributeError:
        pass
    
    censustract_gt_census = (CensusTract.objects.get_caught_pop() and 
                        CensusTract.objects.get_caught_pop() > Census.objects.get_caught_pop())
    if censustract_gt_census:
            lou['total_caught'] = CensusTract.objects.get_caught_pop()
    else:
        lou['total_caught'] = Census.objects.get_caught_pop()

    if request.user.is_staff:
        for site, status in UploadData.objects.most_recent_status_by_site().items():
            lou[status] = lou.get(status, [])
            lou[status].append(site)
        statuses = list(lou.keys())
        if "Pending" in statuses and "Success" in statuses and "Running" not in statuses:
            lou["Waiting"] = lou.pop("Success")

    return lou


def ranged_stratifiers(request):
    return {"ranged_stratifiers": ["age_group", "tg", "ldl", "bmi_percent", "sbp_percent", "dbp_percent", "a1c"]}


def graphable_selectors(request):
    if not settings.ETHNICITY_ENABLED:
        static_stratifications = [
            {"name": "none", "label": "Unstratified"},
            {"name": "age_group", "label": "Age"},
            {"name": "sex", "label": "Sex"},
            {"name": "race_ethnicity", "label": "Race"},
            {"name": "birth_cohort", "label": "Calendar year at birth"},
            {"name": "site", "label": "Practice Group"},
            {"name": "primary_payer", "label": "Primary Payer"}
        ]
    else:
        static_stratifications = [
            {"name": "none", "label": "Unstratified"},
            {"name": "age_group", "label": "Age"},
            {"name": "sex", "label": "Sex"},
            {"name": "race_ethnicity", "label": "Race"},
            {"name": "ethnicity", "label": "Ethnicity"},
            {"name": "birth_cohort", "label": "Calendar year at birth"},
            {"name": "site", "label": "Practice Group"},
            {"name": "primary_payer", "label": "Primary Payer"}
        ]
    location_list = {
                     "zips": Census.objects.values_list('zip', flat=True),
                     "tracts": CensusTract.objects.values_list('tract', flat=True),
                     "cities": City.objects.values_list('city', flat=True),
                     }

    return {"static_stratifications": static_stratifications, "location_list": location_list}


def date_range(request):
    try:
        drm, drq, dry, start, end = get_data_time_range()

        months = [{'display': m.format("MMM `YY"), 'quarter': get_quarter(m.month), 'month': m.month, 'year': m.year}
                  for
                  m in drm]
        quarters = [{'display': "{} `{}".format(get_quarter(q.month), q.format('YY')), 'quarter': get_quarter(q.month),
                     'month': q.month, 'year': q.year} for q in drq]
        years = [{'display': y.format('YYYY'), 'quarter': 'Q1', 'month': 1, 'year': y.year} for y in dry]

        context = {'month_range': months, 'quarter_range': quarters, 'year_range': years}
        context['default_start'] = {'quarter': get_quarter(start.month), 'month': start.month, 'year': start.year}
        context['default_end'] = {'quarter': get_quarter(end.month), 'month': end.month, 'year': end.year}

        return context
    except:
        return {}


def organization(request):
    return {'ORGANIZATION': settings.ORGANIZATION}


def org_system(request):
    return {'ORG_SYSTEM': settings.ORG_SYSTEM}


def coverage_area(request):
    return {'COVERAGE_AREA': settings.COVERAGE_AREA}


def about_text(request):
    return {'ABOUT_TEXT': settings.ABOUT_TEXT}

def google_id(request):
    return {'GOOGLE_TRACKING_ID': settings.GOOGLE_TRACKING_ID}

def google_form(request):
    return {'GOOGLE_FORM': settings.GOOGLE_FORM}

def feedback_text(request):
    return {'FEEDBACK_TEXT': settings.FEEDBACK_TEXT}

def saml_sso_enabled(request):
    return {'SAML_SSO_ENABLED': settings.SAML_SSO_ENABLED}

def min_prev_numerator(request):
    return {'MIN_PREV_NUMERATOR': settings.MIN_PREV_NUMERATOR}

def ethnicity_enabled(request):
    return {'ETHNICITY_ENABLED': settings.ETHNICITY_ENABLED}
