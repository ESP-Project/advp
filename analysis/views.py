from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect, get_object_or_404, redirect, HttpResponse
from django.template.response import TemplateResponse
from django.conf import settings

from analysis import jobs
from analysis.models import MapResultCache, CurrentRateResultCache, TimeseriesResultCache
from analysis.view_utils import current_rates, timeseries, _graph_view, unpack_params, conditional_decorator
from . import forms
from django_otp.decorators import otp_required
from riskscape_registration.models import Profile

import sys, csv
from datetime import datetime

@login_required
def home(request):
    if not settings.TWO_FACTOR_ENABLED or request.user.is_verified():
        outcome_form, filter_form, geo_form = forms.process_queryform(request)

        return render(request, 'analysis/home.html',
            {"landing": True, 
            "outcome_form": outcome_form, 
            "filter_form": filter_form, 
            "geo_form":geo_form})
    elif settings.TWO_FACTOR_ENABLED:
        return redirect('two_factor:setup')

@login_required
def about(request):
    outcome_form, filter_form, geo_form = forms.process_queryform(request)
    context = {"about": True}
    if outcome_form is not None:
        if outcome_form.is_valid() and filter_form.is_valid():
            pass
        context['outcome_form'] = outcome_form
        context['filter_form'] = filter_form
    return TemplateResponse(request, 'analysis/about.html', context)


@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def heat_map(request, outcome=None):
    chunked = unpack_params(request, 'chunked', 'month')
    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year) or (ebm != default_end_month)
    year = eby if update_time else None
    month = ebm if update_time else None
    dynamic_start = None
    dynamic_end = None
    if sby is not None:
        dynamic_start = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        dynamic_end = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    outcome_form, filter_form, geo_form = forms.process_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    geo_place_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass
    mrc = MapResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    geo_form=geo_form,
                                                    month=month,
                                                    year=year)
    if mrc is None or mrc.result is None:
        mrc = MapResultCache.objects.cache_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form, 
                                                    geo_form= geo_form,
                                                    result=None,
                                                    month=month,
                                                    year=year)
        jobs.generate_map_cache.delay(mrc.pk, geo_area, month, year)

    return TemplateResponse(request, 'analysis/map.html',
                            {"map": True,
                             "map_cache": mrc.pk,
                             "outcome_form": outcome_form,
                             "filter_form": filter_form,
                             "geo_form":geo_form,
                             "geo_area":geo_area,
                             "geo_place_type":geo_place_type,
                             "chunked": chunked,
                             "dynamic_start":dynamic_start,
                             "dynamic_end":dynamic_end})


@login_required
def map_overlay(request, pk):
    if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
        if request.method == 'GET':
            mrc = get_object_or_404(MapResultCache, pk=pk)
            if mrc.result is not None:
                return JsonResponse({"complete": True, "data": mrc.result})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)


@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def graph_view(request, graph_type, outcome=None):
    outcome_form, filter_form, geo_form = forms.process_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    geo_place_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass

    if geo_area == 'zip' or geo_area == 'tract':
        geo_location = settings.GEO_LOCATION
    else:
        geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
    default = geo_location + ',' + geo_location

    data = [{'location': ''}, {'location': ''}]
    locations = unpack_params(request, 'locations', default).split(',')
    data[0]['location'] = locations[0]
    try:
        data[1]['location'] = locations[1]
    except IndexError:
        data[1]['location'] = locations[0]

    stratify = unpack_params(request, 'stratify',
                             'age_group' if graph_type == "current" else 'none')
    stratify2 = unpack_params(request, 'stratify2', 'none')

    chunked = unpack_params(request, 'chunked', 'month')


    context = {"stratify": stratify, 
               "stratify2": stratify2,
               "outcome_form": outcome_form, 
               "filter_form": filter_form,
               "geo_form": geo_form,
               "geo_area":geo_area,
               "geo_place_type":geo_place_type,
               "geo_location":geo_location}

    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year) or (ebm != default_end_month)
    year = eby if update_time else None
    month = ebm if update_time else None
    if sby is not None:
        context['dynamic_start'] = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        context['dynamic_end'] = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    try:
        if graph_type == "current":
            context["current"] = True
            context['data'] = current_rates(data, outcome_form, filter_form, geo_form, 
                                            stratify, stratify2, month, year)
            context['chunked'] = chunked
            template = 'analysis/current.html'
        elif graph_type == "timeseries":
            context['timeseries'] = True
            context['data'] = timeseries(data, outcome_form, filter_form, geo_form, stratify, chunked)
            context['chunked'] = chunked

            template = 'analysis/timeseries.html'
        else:
            return redirect(reverse('home'))
    except AttributeError as e:
        return redirect(reverse('home'))

    tr = TemplateResponse(request, template, context)

    return tr


@login_required
def current_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, CurrentRateResultCache)


@login_required
def timeseries_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache)


@login_required
def timeseries_trend(request, inflection, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache, inflection)

@login_required
def user_report_csv(request):
    if not request.user.is_superuser:
        return HttpResponseBadRequest('')
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
    content_type='text/csv',
    headers={'Content-Disposition': 'attachment; filename="user_report.csv"'},
    )
    # write csv header
    writer = csv.writer(response)
    writer.writerow(['full name','institution','email', 'active', 'staff', 'superuser', 'last login date',])
    # write user info
    User = get_user_model()
    users = User.objects.all()
    for u in users:
        p = Profile.objects.get(user=u)
        user_last_login_date = u.last_login.strftime("%Y-%m-%d") if u.last_login else None
        if u.username:
            writer.writerow([
                        p.full_name,
                        p.institution,
                        u.email,
                        u.is_active,
                        u.is_staff,
                        u.is_superuser,
                        user_last_login_date,
                        ])

    # write download date
    writer.writerow([])
    now = datetime.now().strftime("%Y-%m-%d")
    writer.writerow(['Download Date: ', now])

    return response

