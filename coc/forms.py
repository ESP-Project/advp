import arrow
from django import forms

from coc.models import UploadCOC


class UploadCOCForm(forms.ModelForm):
    class Meta:
        model = UploadCOC
        fields = ['data_file', 'site_name', 'coc_disease']


def get_year_choices(choices):
    return choices + [(str(y + 1), str(y + 1)) for y in range(2009, arrow.now().year)]


def get_site_choices(choices, disease):
    return choices + [(s.site_name, s.get_site_name_display()) for s in UploadCOC.objects.filter(coc_disease=disease)]


class COCForm(forms.Form):
    ATTRS = {'class': 'form-check-input small sr-only'}
    site = forms.ChoiceField(label='Site',
                             choices=(('x', 'All'),),
                             widget=forms.CheckboxSelectMultiple(attrs=ATTRS))
    year = forms.ChoiceField(label='Year', choices=(('YEAR', 'All'),),
                             widget=forms.Select(attrs={'class': 'custom-select form-control form-control-sm text-primary'}))
    age_group = forms.ChoiceField(label='Age Group',
                                  choices=(('x', 'All'), ('1', '0-19'), ('2', '20-39'), ('3', '40-59'), ('4', '60+'),
                                           ('5', 'Unknown')),
                                  widget=forms.CheckboxSelectMultiple(attrs=ATTRS))
    birth_cohort = forms.ChoiceField(label='Birth Cohort',
                                     choices=(('x', 'All'), ('1', 'Before 1945'), ('2', '1945 to 1965'),
                                              ('3', 'After 1965'), ('4', 'Unknown')),
                                     widget=forms.CheckboxSelectMultiple(attrs=ATTRS))
    sex = forms.ChoiceField(label="Sex", choices=(('x', 'All'), ('1', 'Female'), ('2', 'Male'), ('3', 'Unknown')),
                            widget=forms.CheckboxSelectMultiple(attrs=ATTRS))
    race_ethnicity = forms.ChoiceField(label="Race/Ethnicity",
                                       choices=(('x', 'All'), ('1', 'Asian'), ('2', 'Black'), ('3', 'Caucasian'),
                                                ('4', 'Hispanic'), ('5', 'Other'), ('6', 'Unknown')),
                                       widget=forms.CheckboxSelectMultiple(attrs=ATTRS))
    risk_percentile = forms.ChoiceField(label="10 Year Risk",
                                        choices=(
                                            ('x', 'All'), ('1', 'Has Disease'), ('2', '20% or more'), ('3', '15%-20%'),
                                            ('4', '10%-15%'), ('5', '5%-10%'), ('6', '0%-5%')
                                        ),
                                        widget=forms.CheckboxSelectMultiple(attrs=ATTRS))

    def __init__(self, disease_slug, *args, **kwargs):
        super(COCForm, self).__init__(*args, **kwargs)
        self.fields['year'].choices = get_year_choices(self.fields['year'].choices)
        self.fields['site'].choices = get_site_choices(self.fields['site'].choices, disease_slug)
