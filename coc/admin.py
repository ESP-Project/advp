from django.contrib import admin

from coc.models import UploadCOC, COCDisease

admin.site.register(UploadCOC)
admin.site.register(COCDisease)
