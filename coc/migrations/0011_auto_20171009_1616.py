# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import coc.models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0010_auto_20171009_1559'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cocdisease',
            name='id',
        ),
        migrations.AlterField(
            model_name='cocdisease',
            name='slug',
            field=models.CharField(serialize=False, max_length=5, primary_key=True),
        ),
        migrations.AlterField(
            model_name='uploadcoc',
            name='data_file',
            field=models.FileField(upload_to=coc.models.path_and_rename, max_length=255, storage=coc.models.OverwriteStorage()),
        ),
    ]
