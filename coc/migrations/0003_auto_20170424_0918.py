# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0002_auto_20170328_0654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadcoc',
            name='site_name',
            field=models.CharField(max_length=3, choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'MLCHC')]),
        ),
    ]
