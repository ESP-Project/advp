import os

from datetime import datetime
from django.core.files.storage import FileSystemStorage
from django.db import models

from patient_data import model_choices


class OverwriteStorage(FileSystemStorage):
    pass#Keep old migration happy

def path_and_rename(instance, filename):
    upload_to = 'coc'
    filename = '{}_{}-{}.json'.format(instance.coc_disease.slug, instance.site_name, datetime.now().strftime("%s"))
    return os.path.join(upload_to, filename)


class UploadCOC(models.Model):
    data_file = models.FileField(max_length=255, upload_to=path_and_rename)
    site_name = models.CharField(max_length=3, choices=model_choices.SITE_CHOICES, blank=False, null=False)
    coc_disease = models.ForeignKey('COCDisease', related_name="files", blank=False, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return "{}:{}".format(self.get_site_name_display(), self.coc_disease.name)

    def save(self, *args, **kwargs):
        UploadCOC.objects.filter(site_name=self.site_name, coc_disease=self.coc_disease).delete()
        super(UploadCOC, self).save(*args, **kwargs)

    class Meta:
        ordering = ['site_name']


class COCDisease(models.Model):
    name = models.CharField(max_length=50)
    slug = models.CharField(max_length=10, primary_key=True)

    def __str__(self):
        return self.name
