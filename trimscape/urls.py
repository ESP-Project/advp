from django.urls import include, re_path
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import ( LoginView, LogoutView, 
        PasswordChangeView, PasswordChangeDoneView, 
        PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView )
from riskscape_registration.views import RiskscapeRegistrationView 
from rest_framework_simplejwt.views import TokenObtainPairView

from analysis import views as analysis_views
from coc import views as coc_views
from patient_data import views as pd_views
from sso import views as sso_views
from trimscape import settings

from two_factor.urls import urlpatterns as tf_urls
from two_factor.views import SetupCompleteView

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^accounts/logout/$', LogoutView.as_view(),  name='logout'),
    re_path(r'^accounts/password_change/$', 
        PasswordChangeView.as_view(template_name='trimscape/password_change_form.html'), 
        name='password_change'),
    re_path(r'^accounts/password_change_done/$', 
        PasswordChangeDoneView.as_view(template_name='trimscape/password_change_done.html'), 
        name='password_change_done'),
    re_path(r'^accounts/password_reset/$', 
        PasswordResetView.as_view(template_name='trimscape/password_reset_form.html'), 
        name='password_reset'),
    re_path(r'^accounts/password_reset_done/$', 
        PasswordResetDoneView.as_view(template_name='trimscape/password_reset_done.html'), 
        name='password_reset_done'),
    re_path(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirmView.as_view(template_name='trimscape/password_reset_confirm.html'), 
        name='password_reset_confirm'),
    re_path(r'^accounts/reset/done/$', PasswordResetCompleteView.as_view(template_name='trimscape/password_reset_complete.html'), 
        name='password_reset_complete'),
    re_path(r'^data/$', pd_views.upload_file),
    re_path(r'^data/historic/$', pd_views.upload_historic_file),
    re_path(r'^data/upload/$',pd_views.model_form_upload),
    re_path(r'^$', analysis_views.home, name='home'),
    re_path(r'^(?P<outcome>[\w ]+)$', analysis_views.heat_map, name='outcome'),
    re_path(r'^map/$', analysis_views.heat_map, name='map'),
    re_path(r'^map/(?P<pk>\d+)/$', analysis_views.map_overlay, name='map_overlay'),
    re_path(r'^current/$', analysis_views.graph_view, {"graph_type": "current"}, name='current'),
    re_path(r'^current/(?P<outcome>[\w ]+)$', analysis_views.graph_view, {"graph_type": "current"},
        name='current-outcome'),
    re_path(r'^current/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', analysis_views.current_graph, name='current_cache'),
    re_path(r'^timeseries/$', analysis_views.graph_view, {"graph_type" : "timeseries"}, name='timeseries'),
    re_path(r'^timeseries/(?P<outcome>[\w ]+)$', analysis_views.graph_view, {"graph_type": "timeseries"},
        name='timeseries-outcome'),
    re_path(r'^timeseries/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', analysis_views.timeseries_graph, name='timeseries_cache'),
    re_path(r'^trendline/(?P<inflection>\d+)/timeseries/(?P<pk1>\d+)/(?P<pk2>-?-?\d+)/$',
        analysis_views.timeseries_trend, name='timeseries_trendline'),
    re_path(r'^about/$', analysis_views.about, name='about'),
    re_path(r'^coc/$', coc_views.coc_home, name='coc_home'),
    re_path(r'^coc/(?P<slug>[\w ]+)$', coc_views.coc, name='coc'),
    re_path(r'^coc/upload/$', coc_views.process_upload, name='coc-upload'),
    re_path(r'^django-rq/', include('django_rq.urls')),
    re_path(r'^sso/$', sso_views.samlsso, name='samlsso'),
    re_path(r'^accounts/', include('registration.backends.admin_approval.urls')),
    re_path(r'^registration/register/$',
        RiskscapeRegistrationView.as_view(template_name='riskscape_registration/registration_form.html'),
        name='registration_register'),
    re_path(r'^api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^api/upload/', pd_views.UploadView.upload_file, name='upload_file'),
    re_path(r'^user-report/', analysis_views.user_report_csv, name='user_report'),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.TWO_FACTOR_ENABLED:
    login_path = re_path(r'^accounts/login/$', LoginView.as_view(template_name='two_factor/login.html'), name='login')
    urlpatterns.insert(1,login_path)
    urlpatterns.append(
        re_path(
            r'^account/two_factor/setup/complete/$',
            SetupCompleteView.as_view(template_name='two_factor/setup_complete.html'),
            name='setup_complete',
        )
    )
    urlpatterns.append(
        re_path(r'^', include(tf_urls))
    )
else:
    login_path = re_path(r'^accounts/login/$', LoginView.as_view(template_name='trimscape/login.html'), name='login')
    urlpatterns.insert(1,login_path)
if settings.PYTHON == '3.10':
    urlpatterns.append(
        re_path(r'^scheduler/', include('scheduler.urls'))
    )
